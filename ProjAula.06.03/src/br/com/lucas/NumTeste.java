/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.lucas;

import java.util.Scanner;

/**
 *
 * @author Alunos
 */
public class NumTeste {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int numero_teste;
        
        System.out.print("Informe um número: ");
        numero_teste = scan.nextInt();   
    
    
        if (numero_teste % 2 == 0) {
            System.out.println("O número informado é par");
        } else {
            System.out.println("O número informado é impar");
        }
    }

}
