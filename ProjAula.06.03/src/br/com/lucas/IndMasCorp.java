/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.lucas;

import java.util.Scanner;

public class IndMasCorp {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        double peso, altura;

        System.out.print("Informe seu peso: ");
        peso = scan.nextDouble();

        System.out.print("Informe sua altura: ");
        altura = scan.nextDouble();

        double imc = (peso / (altura * altura));

        if (imc < 17) {
            System.out.println("Seu IMC é " + imc + ", você está muito abaixo do peso");
        } else if (imc > 17 & imc < 18.49) {
            System.out.println("Seu IMC é " + imc + ", você está abaixo do peso");
        } else if (imc > 18.5 & imc < 24.99) {
            System.out.println("Seu IMC é " + imc + ", você está com o peso normal");
        } else if (imc > 25 & imc < 29.99) {
            System.out.println("Seu IMC é " + imc + ", você está acima do peso");
        } else if (imc > 30 & imc < 34.99) {
            System.out.println("Seu IMC é " + imc + ", você está com Obesidade I");
        } else if (imc > 35 & imc < 39.99) {
            System.out.println("Seu IMC é " + imc + ", você está com Obesidade II");
        } else {
            System.out.println("Seu IMC é " + imc + ", você está com Obesidade Mórbida");
        }
    }
}

